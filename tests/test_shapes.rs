extern crate rust_ex;

use rust_ex::shapes::{Circle, Rectangle, Shape};
use std::f64;

#[test]
fn test_rectangles() {
    let r = Rectangle { a: 2.0, b: 4.0 };
    assert!(r.perimeter() == 12.0);

    let c = Circle { radius: 5.0 };
    //assert!(c.perimeter() == 15.0);
}
