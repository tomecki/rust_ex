extern crate rust_ex;
use rust_ex::unique::unique;

#[test]
pub fn test_unique() {
    let input = vec![1, 2, 3, 2, 5, 4, 5, 6, 10];
    println!("{:?}", input);
    let output: Vec<u32> = unique(input).collect();
    assert_eq!(output, vec![1, 2, 3, 5, 4, 6, 10]);
    println!("{:?}", output);
}
