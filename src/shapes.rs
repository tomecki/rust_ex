use std::f64;

pub trait Shape {
    fn area(&self) -> f64;
    fn perimeter(&self) -> f64;
}

pub struct Circle {
    pub radius: f64,
}

pub struct Rectangle {
    pub a: f64,
    pub b: f64,
}

impl Shape for Circle {
    fn area(&self) -> f64 {
        self.radius * self.radius * f64::consts::PI
    }
    fn perimeter(&self) -> f64 {
        2.0 * f64::consts::PI * self.radius
    }
}

impl Shape for Rectangle {
    fn area(&self) -> f64 {
        self.a * self.b
    }
    fn perimeter(&self) -> f64 {
        2.0 * (self.a + self.b)
    }
}
