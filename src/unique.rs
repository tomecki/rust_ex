use std::collections::HashSet;
use std::hash::Hash;

pub struct UniqueIterator<T> {
    inner_iterator: ::std::vec::IntoIter<T>,
    unique_elems: HashSet<T>,
}

impl<T> Iterator for UniqueIterator<T>
where
    T: Eq + Hash + Copy,
{
    type Item = T;
    fn next(&mut self) -> Option<Self::Item> {
        while let Some(elem) = self.inner_iterator.next() {
            if !self.unique_elems.contains(&elem) {
                self.unique_elems.insert(elem);
                return Some(elem);
            }
        }
        None
    }
}

pub fn unique<T>(input_vec: Vec<T>) -> UniqueIterator<T>
where
    T: Sized + Eq + Hash,
{
    UniqueIterator {
        inner_iterator: input_vec.into_iter(),
        unique_elems: HashSet::new(),
    }
}
